const robot = require("robotjs");
const fs = require("fs");
const Jimp = require("jimp");
const PushBullet = require("./pushbullet");
const config = require("./config.json");

const tesseract = require("node-tesseract-ocr")

function captureImage(x, y, w, h) {
    const pic = robot.screen.capture(x, y, w, h)
    const width = pic.byteWidth / pic.bytesPerPixel // pic.width is sometimes wrong!
    const height = pic.height
    const image = new Jimp(width, height)
    let red, green, blue
    pic.image.forEach((byte, i) => {
        switch (i % 4) {
            case 0: return blue = byte
            case 1: return green = byte
            case 2: return red = byte
            case 3: 
                image.bitmap.data[i - 3] = red
                image.bitmap.data[i - 2] = green
                image.bitmap.data[i - 1] = blue
                image.bitmap.data[i] = 255
        }
    })
    return image
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}


async function init(){
    let running = true;

    const pushSettings = {
        device_iden: config.pushbullet.device
    }

    const pb = new PushBullet(config.pushbullet.token);

    pb.setupStream();
    

    while(running){
        captureImage(50,300,350,400).write("output.png");

        const tessConfig = {
            lang: "eng",
            oem: 1,
            psm: 3,
        }

        tesseract.recognize("output.png", tessConfig).then(text => {
            console.log("Result: ", text);
            const lowered = text.toLowerCase();
            if(lowered.includes("enter")){
                pb.file(pushSettings, "output.png", "MO2 Queue Update", function(err, res){
                    if(err){
                        console.warn(`Failed to push response: ${err}`);
                    }
                });
            }
        }).catch(err => {
            console.log(err.message);
        })
        await sleep(5000);
    }
}

init();