const PushBullet = require("pushbullet");
const EventEmitter = require("events").EventEmitter;

class PushBulletExtended extends PushBullet {

    events

    constructor(token){
        super(token);
        this.events = new EventEmitter();
    }

    setupStream(){
        const pb = this;
        const pbEvents = pb.stream();

        pbEvents.on("connect", function(){
            console.log("Connected to PushBullet");
        });

        pbEvents.on("tickle", function(type){
            console.log(`Tickled with type: ${type}`);
            if(type === "push"){
                const historyOptions = {
                    limit: 1,
                    modified_after: Math.floor(new Date().getTime()/1000) - 5000
                }

                pb.history(historyOptions, function(err, res){
                    if(err){
                        console.log(`ERROR: ${err}`);
                        return;
                    }

                    //console.log(res);
                    pb.events.emit("push_message", res);
                })
            }
        })

        pbEvents.connect();
    }

}

module.exports = PushBulletExtended;